﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;


using System;
using System.Linq;

using System.Diagnostics;
using System.IO;

public static class MyExtensionsList
{
    public static bool CanMoveUp<T>(this List<T> list, uint lIndex)
    {
        return (list.IsValidIndex(lIndex) && lIndex > 0);
    }
    public static bool CanMoveDown<T>(this List<T> list, uint lIndex)
    {
        return (list.IsValidIndex(lIndex) && lIndex <= list.Count - 2);
    }

    public static void MoveUp<T>(this List<T> list, uint lIndex)
    {
        if (list.CanMoveUp(lIndex) == true)
        {
            list.Swap(lIndex, lIndex - 1);
        }
    }

    public static void MoveDown<T>(this List<T> list, uint lIndex)
    {
        if (list.CanMoveDown(lIndex) == true)
        {
            list.Swap(lIndex, lIndex + 1);
        }
    }

    public static void Swap<T>(this List<T> list, uint lIndex1, uint lIndex2)
    {
        if (lIndex1 == lIndex2)
            return;
        if (list.IsValidIndex(lIndex1) == false || list.IsValidIndex(lIndex2) == false)
        {
            return;
        }
        T aux = list[(int)lIndex1];
        list[(int)lIndex1] = list[(int)lIndex2];
        list[(int)lIndex2] = aux;

    }

    public static bool IsValidIndex<T>(this List<T> list, uint lIndex)
    {
        return (lIndex <= list.Count - 1);
    }
}

public class SimpleDictionaryList<TKey, TValue>
{
    Dictionary<TKey, List<TValue>> m_Dictionary = new Dictionary<TKey, List<TValue>>();
    public SimpleDictionaryList()
    {
        m_Dictionary.Clear();
    }

    /// <summary>
    /// 
    /// <returns></returns>
    public List<TValue> GetList(TKey lTKey)
    {
        m_CheckForKey(lTKey);
        return m_Dictionary[lTKey];
    }

    public int Count(TKey lKey)
    {
        m_CheckForKey(lKey);
        return m_Dictionary[lKey].Count;
    }

    public List<TValue> this[TKey lTKey]
    {
        get
        {
            m_CheckForKey(lTKey);
            return m_Dictionary[lTKey];
        }
        set
        {
            m_CheckForKey(lTKey);
            m_Dictionary[lTKey] = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void ClearAll()
    {
        foreach (TKey lKey in m_Dictionary.Keys)
        {
            m_CheckForKey(lKey);
            m_Dictionary[lKey].Clear();
        }
    }
    /// <summary>
    /// 
    /// <param name="lEDebugType"></param>
    public void ClearList(TKey lTKey)
    {
        m_CheckForKey(lTKey);
        m_Dictionary[lTKey].Clear();
    }

    /// <summary>
    /// 
    /// <param name="lString"></param>
    public void AddToList(TKey lTKey, TValue lTValue)
    {
        m_CheckForKey(lTKey);
        m_Dictionary[lTKey].Add(lTValue);
    }

    private void m_CheckForKey(TKey lTKey)
    {
        if (m_Dictionary.ContainsKey(lTKey) == false)
        {
            m_Dictionary.Add(lTKey, new List<TValue>());
        }
    }

}

