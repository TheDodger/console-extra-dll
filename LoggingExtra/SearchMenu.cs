﻿
using UnityEngine;

using UnityEditor;

using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

// Simple script that creates a new non-dockable window


public class SearchMenu
 : EditorWindow
{

    //--------------------------------------------
    // These will be shown in "Console Extra"
    //--------------------------------------------





    //---------------------------------------------------------------
    // these are the enums that you can use when using ConsoleExtra.Log()
    //---------------------------------------------------------------
    public enum ESearchType
    {
        Message,
        CallStack,
		GameObjectName,
		All,
    }





    static SearchMenu m_Window = null;

    public Consts m_Consts = null;
    static bool m_LoadVariablesOnStart = false;



    

    [System.Serializable]
    public class Consts
    {
        public string m_SearchText = "";

        public ESearchType SearchType = ESearchType.Message;
        public StringExtensions.ECaseSensitive CaseSensive = StringExtensions.ECaseSensitive.CaseSensitive;

        public int SpaceAtBeginning = 50;
        public int NameWidth = 150;
        public int InformationWidth = 150;
        public int  InformationHeight = 20;

        public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo0 = null;
        public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo1 = null;
        public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo2 = null;
        public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo3 = null;
        public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo4 = null;
        public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo5 = null;
    }




    //--------------------------------------------------
    // these are what apear in the console its self
    //--------------------------------------------------




    public enum ETabStyle
    {
        HighLight,
        Normal,
        NewItem,
        NoNewItem,
    }

    public enum EDisplayTime
    {
        NoTime,
        FromStart,
        RealTime,
        RealTimeShort,
    }

    public enum EDisplayGameObjectName
    {
        NoName,
        Name,
        FullName,
    }
    //-------------------------------------------------------------------
    // when in #if !UNITY_EDITOR mode then it will log only these enums
    //-------------------------------------------------------------------




    Logging m_Logging;
    public  SearchMenu(Logging lLogging)
    {
        m_Window = (SearchMenu)EditorWindow.GetWindow(typeof(SearchMenu), true, "Search Menu");
        m_Window.maxSize       = new Vector2(215f * 2, 110f * 2);
        m_Window.minSize       = m_Window.maxSize;
        m_LoadVariablesOnStart = false;
        m_Logging              = lLogging;
        m_Consts               = new Consts();
        m_Consts.m_SearchText  = "";
        m_LoadVariablesOnStart = false;
    }

    /// <summary>
    /// 
    /// </summary>
    static void Save()
    {
    }


    /// <summary>
    /// 
    /// </summary>
    static void Load()
    {
    }

    /// <summary>
    /// 
    /// </summary>
    private void OnGUI()
    {
		if(m_Logging == null)
		{
			this.Close();
        }
        if (m_Consts != null)
        {
            OnInspectorUpdate();
            if (m_LoadVariablesOnStart == true)
            {
                FillInWindowSetting();
            }
        }
        Repaint();
		Logging.m_Repaint = true;
    }

    /// <summary>
    /// 
    /// </summary>
    void OnInspectorUpdate()
    {
		if(m_Logging == null)
		{
			this.Close();
        }
        if (m_Window == null)
        {
            m_Window = (SearchMenu)EditorWindow.GetWindow(typeof(SearchMenu), false, "Console Extra");
        }

        if (m_LoadVariablesOnStart == false)
        {
            InitiliseGuiFormat();
            Load();
            m_LoadVariablesOnStart = true;
        }
        Repaint();
		Logging.m_Repaint = true;
    }




    /// <summary>
    /// 
    /// <returns></returns>

    void InitiliseGuiFormat()
    {
        m_Consts.GuiInfo0 = new Helper.EditorGUILayout.EditorGUILayoutInfo(0, m_Consts.SpaceAtBeginning * 0, 0, 10, m_Consts.InformationHeight);
        m_Consts.GuiInfo1 = new Helper.EditorGUILayout.EditorGUILayoutInfo(1, m_Consts.SpaceAtBeginning * 0, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
        m_Consts.GuiInfo2 = new Helper.EditorGUILayout.EditorGUILayoutInfo(1, m_Consts.SpaceAtBeginning * 1, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
        m_Consts.GuiInfo3 = new Helper.EditorGUILayout.EditorGUILayoutInfo(1, m_Consts.SpaceAtBeginning * 2, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
        m_Consts.GuiInfo4 = new Helper.EditorGUILayout.EditorGUILayoutInfo(1, m_Consts.SpaceAtBeginning * 3, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
        m_Consts.GuiInfo5 = new Helper.EditorGUILayout.EditorGUILayoutInfo(1, m_Consts.SpaceAtBeginning * 4, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
    }
    /// <summary>
    /// 
    /// </summary>
    void FillInWindowSetting()
    {
		if(m_Logging == null)
		{
			this.Close();
        }
        GUI.backgroundColor = Color.white;

        Helper.EditorGUILayout.Space(3);

        m_Consts.SearchType   = (ESearchType)Helper.EditorGUILayout.PopupEnum("Message Type", Enum.GetNames(typeof(ESearchType)), (int)m_Consts.SearchType, m_Consts.GuiInfo1);
        m_Consts.CaseSensive  = (StringExtensions.ECaseSensitive)Helper.EditorGUILayout.PopupEnum("CaseSensive Type", Enum.GetNames(typeof(StringExtensions.ECaseSensitive)), (int)m_Consts.CaseSensive, m_Consts.GuiInfo1);
        m_Consts.m_SearchText = Helper.EditorGUILayout.DelayedTextField("String" , m_Consts.m_SearchText, m_Consts.GuiInfo1);
    }


}


