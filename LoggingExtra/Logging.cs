﻿using UnityEngine;

using UnityEditor;
using ConsoleExtraEnum;

using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;




public class LoggingExtra 
{
    static Logging m_ConsoleInstance;
    public static void Log(string lMessage, GameObject lGameObject, ConsoleExtraEnum.EDebugType lEDebugType)
    {
        if (object.ReferenceEquals(m_ConsoleInstance,null))
        {
            m_ConsoleInstance = new Logging();
            m_ConsoleInstance.m_Consts = new Logging.Consts();

            m_ConsoleInstance.CreateListOfFonts();
            m_ConsoleInstance.InitiliseEDebugTypeBaseFonts();
            m_ConsoleInstance.InilisizeTabTyle();
            m_ConsoleInstance.Load();
            m_ConsoleInstance.InitiliseGuiFormat();
        }

        m_ConsoleInstance.AddToMessages(m_ConsoleInstance, lMessage, lEDebugType, lGameObject);
        Logging.m_Repaint = true;
    }
}


    [InitializeOnLoad]
public class Logging: EditorWindow
{
	
    #region varaibles
    static string LoggingString = "Logging_";

    static Logging        m_ConsoleInstance;
    public static Logging m_Window                = null;
    static EDebugTypeBase m_EDebugTypeBase        = EDebugTypeBase.All;
    internal Consts       m_Consts                = null;
    static bool           m_LoadVariablesOnStart  = false;
    public static bool    m_Repaint               = true;
	
    static SimpleDictionaryList<EDebugTypeBase, Data> m_DictionaryListData = new SimpleDictionaryList<EDebugTypeBase, Data>();
    static Dictionary<EDebugTypeBase, int> m_DictionaryListLastViewdCount  = new Dictionary<EDebugTypeBase, int>();

	static SimpleDictionaryList<EDebugTypeBase, Data> m_DictionaryListCountData = new SimpleDictionaryList<EDebugTypeBase, Data>();




	static void AddDictionaryListCountData(EDebugTypeBase lEDebugTypeBase, Data lData)
	{
		bool lFound      = false;
		List<Data> lList = m_DictionaryListCountData.GetList(lEDebugTypeBase);
		Data lItem       = null;
		for(int i = 0; i < lList.Count; i++)
		{
			lItem = lList[i];
			if(lItem.Equals(lData) == true)
			{
				lFound = true;
				break;
			}
		}

		if(lFound == true)
		{
			lItem.m_Count++;
		}
		else
		{
			lList.Add(lData);
            lList[lList.Count - 1].m_Count = 1;
		}
	}

	//
    [System.Serializable]
    internal class Consts
    {
		public bool AudioTracking = false;
        public Data SelectedItem = null;
		public bool Compressed = false;
        public char LineSplitter      = '\n';
        public string ClearButtonText = "Clear Data";
		public Color StoreColour      = Helper.Colour.Colour256ToColour(11, 196, 89); // nice green
        public string CSC_Directory  = @"C:\Windows\Microsoft.NET\Framework64\v3.5\csc.exe";
        public string STAY_CSC_Directory = @"C:\Windows\Microsoft.NET\Framework64\v3.5\csc.exe";
        public bool SelectEmptyItem   = false;
        public int StartSpace         = 10;


        // ----------------------------------------------------------
        //          Font Data
        // ----------------------------------------------------------
        public Font[] Fonts             = null;
        public List<string> FontStrings = new List<string>();
        public FontData TabFont         = new FontData(Color.black);
        public Dictionary<ETabStyle, FontData> TabStyle = new Dictionary<ETabStyle, FontData>();


        public ETabStyle TabStyleIndex              = ETabStyle.Normal;

        public EDebugTypeBase EDebugTypeBaseFontsChoice = EDebugTypeBase.All;
		public Dictionary<EDebugTypeBase, EDebugTypeBaseFont> EDebugTypeBaseFonts = new Dictionary<EDebugTypeBase, EDebugTypeBaseFont>();

		public Vector2 m_ScrollPos    = Vector2.zero;
		public Vector2 m_ScrollPosTab = Vector2.zero;
        public Vector2 m_ScrollPosCallStack = Vector2.zero;
        public int EnumAllFlag = int.MaxValue;
        public Dictionary<EDebugTypeBase, bool> EDebugTypeAllList = new Dictionary<EDebugTypeBase, bool>();

        public EDisplayTime DisplayTime = EDisplayTime.FromStart;


        public int ButtonHeight     = 50;
        public int ButtonStartWidth = 25;
        public int ButtonWidth      = 150;

        public int CurrentIndex = 1;



		public int SpaceAtBeginning  = 50;
		public int NameWidth         = 150;
		public int InformationWidth  = 150;
        public int InformationHeight = 25;

        public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo0 = null;
		public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo1 = null;
		public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo2 = null;
		public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo3 = null;
		public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo4 = null;
		public Helper.EditorGUILayout.EditorGUILayoutInfo GuiInfo5 = null;
        public int PadRightTimeGameobject = 20;
        public int SelectButtonGap        = 10;
    }

	/// <summary>
	/// E debug type base font.
	/// </summary>
	public class EDebugTypeBaseFont
    {
        public FontData Message = new FontData(Color.red);
        public FontData CallStack = new FontData(Color.white);
    }
	
	/// <summary>
	/// Font data.
	/// </summary>
    public class FontData
    {
        public int FontIndex = 0;
        public int FontSize  = 10;
        public Font Font     = null;
        public FontStyle FontStyle = FontStyle.Normal;
        public Color Colour  = Color.black;
        public FontData() {}
        public FontData(Color lColor) { Colour = lColor;}
    }

    //--------------------------------------------------
    // these are what apear in the console its self
    //--------------------------------------------------



	/// <summary>
	/// E tab style.
	/// </summary>
    public enum ETabStyle
    {
        HighLight,
        Normal,
        NewItem,
        NoNewItem,
    }
	
	/// <summary>
	/// E display time.
	/// </summary>
    public enum EDisplayTime
    {
        NoTime,
        FromStart,
        RealTime,
        RealTimeShort,
    }

	/// <summary>
	/// E display game object name.
	/// </summary>
	public enum EDisplayGameObjectName
	{
		NoName,
		Name,
		FullName,
	}
    //-------------------------------------------------------------------
    // when in #if !UNITY_EDITOR mode then it will log only these enums
    //-------------------------------------------------------------------
	

	/// <summary>
	/// Data.
	/// </summary>
    public class Data
    {
		public int            m_Count              = 1;
        public int            m_Index              = 0;
        public float          m_TimeFromStart      = 0;
        public DateTime       m_DateTime;
        public string         m_Message            = "";
        public string         m_CallStack          = "";
		public EDebugTypeBase m_EDebugType         = EDebugTypeBase.All;
		public string         m_GameObjectNameFull = "";
        public GameObject     m_GameObject         = null;
		public Data(int lIndex, float lTime, DateTime lDateTime, string lMessage, string lCallStack, EDebugTypeBase lEDebugType,GameObject lGameObject)
        {
            m_Index         = lIndex;
            m_TimeFromStart = lTime;
            m_DateTime      = lDateTime;
            m_Message       = lMessage;
            m_CallStack     = lCallStack;
            m_EDebugType    = lEDebugType;
            m_GameObject    = lGameObject;
			m_Count 		= 0;
            m_GameObjectNameFull = "NULL";
            if(object.ReferenceEquals(m_GameObject,null) == false)
            {
                m_GameObjectNameFull = Helper.Generail.GetGameObjectPath(lGameObject);
            }  
        }


		public  bool Equals( Data lOther )
		{
            if (object.ReferenceEquals(this.m_GameObject, lOther.m_GameObject) == false) return false;
			if (this.m_Message       != lOther.m_Message)       return false;
			if (this.m_CallStack 	 != lOther.m_CallStack)     return false;
			return true;            
		}

    }
    #endregion

    /// <summary>
    /// 
    /// <returns></returns>


    /// <summary>
    /// 
    /// 



	internal void AddToMessages(Logging lConsoleInstance, string lMessage, EDebugType lEDebugType, GameObject lGameObject)
	{
        lConsoleInstance.AddToMessages(lMessage,(EDebugTypeBase)lEDebugType,lGameObject);
	}
    /// <summary>
    /// 
	internal void AddToMessages(string lMessage, EDebugTypeBase lEDebugType, GameObject lGameObject)
    {        
        // -------------------------------------------------------------
        //              Clean up Call stack
        //--------------------------------------------------------------
        string lStack    = System.Environment.StackTrace;
        string[] lStacks = lStack.Split(m_Consts.LineSplitter);
        string lNewStack = "";

        string lLoggingFind = "Logging.cs";
        int lStartIndex = 0;
        // -------------------------------------------------------------
        //              Only Valid call stack parts
        //--------------------------------------------------------------
        for (int i = 0; i < lStacks.Length; i++)
        {
            if(lStacks[i].Contains(lLoggingFind) == true)
            {
                lStartIndex = i + 1; // will check all , incase it finds more than 1 of them , depending on my code 
            }
        }

        for (int i = lStartIndex; i < lStacks.Length; i++)
        {
            lNewStack += lStacks[i] + m_Consts.LineSplitter;
        }

        float lTime = -1;
        if (object.ReferenceEquals(lGameObject, null) == false)
        {
            lTime = Time.realtimeSinceStartup;
        }

        Data lNewData    = new Data(m_Consts.CurrentIndex, lTime, DateTime.Now, lMessage, lNewStack, lEDebugType, lGameObject);
        m_DictionaryListData.AddToList((EDebugTypeBase)lEDebugType, lNewData);
		AddDictionaryListCountData((EDebugTypeBase)lEDebugType, lNewData);
        
        //Hack not needed if (Helper.Enum.IsEnumInEnumItem<EDebugSpecailList, EDebugTypeBase>(lEDebugType) == false)
		{
            m_DictionaryListData[EDebugTypeBase.All].Add(lNewData);
            AddDictionaryListCountData(EDebugTypeBase.All, lNewData);
		}
        m_Consts.CurrentIndex++;

    }

    /// <summary>
    /// 
    /// </summary>
    void Remake_TabAll()
    {
        List<Data> lNewData = new List<Data>();


        foreach (var lItem in m_Consts.EDebugTypeAllList)
        {
            if (lItem.Value == true)
            {
                List<Data> lData = m_DictionaryListData.GetList(lItem.Key);
                for (int i = 0; i < lData.Count; i++)
                {
                    lNewData.Add(lData[i]);

                }
            }
        }

        lNewData = lNewData.OrderBy(o => o.m_Index).ToList();
        m_DictionaryListData[EDebugTypeBase.All] = lNewData;


        //-------------------------------------------------------------------
        m_DictionaryListCountData[EDebugTypeBase.All].Clear();
        for (int i = 0; i < lNewData.Count; i++)
        {
            AddDictionaryListCountData(EDebugTypeBase.All, lNewData[i]);
        }
    }



    #region Display Console

    [MenuItem("Tools/Console Extra  %#&E")]
    static void Initialize()
    {
        if (m_Window == null)
        {
            m_Window = (Logging)EditorWindow.GetWindow(typeof(Logging), false, "Console Extra");
        }
        if (m_ConsoleInstance == null)
        {
            m_ConsoleInstance          = new Logging();
            m_ConsoleInstance.m_Consts = new Consts();

            m_ConsoleInstance.CreateListOfFonts();
            m_ConsoleInstance.InitiliseEDebugTypeBaseFonts();
            m_ConsoleInstance.InilisizeTabTyle();
            m_ConsoleInstance.Load();
            m_ConsoleInstance.InitiliseGuiFormat();
        }
    }


    /// <summary>
    /// 
    /// </summary>   
    static Logging()
    {
        //Selection.selectionChanged += OnSelectionChange;
        m_EDebugTypeBase         = EDebugTypeBase.All;
        
        m_LoadVariablesOnStart   = false;
        m_Repaint                = true;
        ClearList();
    }


    /// <summary>
    /// 
    /// </summary>
    static void ClearList()
    {
        foreach (EDebugTypeBase lEnumItem in Enum.GetValues(typeof(EDebugTypeBase)))
        {
            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
            if (m_DictionaryListLastViewdCount.ContainsKey(lEnumItem) == false)
            {
                m_DictionaryListLastViewdCount[lEnumItem] = 0;
            }
            m_DictionaryListData.ClearList(lEnumItem);
			m_DictionaryListCountData.ClearList(lEnumItem);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void Save()
    {
        PlayerPrefsX.SetColor(LoggingString + "StoreColour",      m_Consts.StoreColour);
        PlayerPrefsX.SetBool (LoggingString + "SelectEmptyItem",  m_Consts.SelectEmptyItem);
        PlayerPrefs.SetInt(LoggingString  + "DisplayTime",   (int) m_Consts.DisplayTime);
        Save_CscFile();
        PlayerPrefs.SetString(LoggingString + "CSC_Directory", m_Consts.CSC_Directory);

        PlayerPrefs.SetInt(LoggingString + "TabFont.FontIndex",      m_Consts.TabFont.FontIndex);
        PlayerPrefs.SetInt(LoggingString + "TabFont.FontSize",       m_Consts.TabFont.FontSize);
        PlayerPrefs.SetInt(LoggingString + "TabFont.FontStyle", (int)m_Consts.TabFont.FontStyle);

        foreach (EDebugType lEnumItemTemp in Enum.GetValues(typeof(EDebugType)))
        {
			EDebugTypeBase lEnumItem = (EDebugTypeBase)lEnumItemTemp;
            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
			if (m_Consts.EDebugTypeBaseFonts.ContainsKey(lEnumItem) == false)
            {
				m_Consts.EDebugTypeBaseFonts.Add(lEnumItem, new EDebugTypeBaseFont());
            }
			FontData lMessage = m_Consts.EDebugTypeBaseFonts[lEnumItem].Message;
            PlayerPrefs.SetInt   (LoggingString + lEnumItem + "_Message.FontSize",  lMessage.FontSize);
            PlayerPrefsX.SetColor(LoggingString + lEnumItem + "_Message.Colour",    lMessage.Colour);
            PlayerPrefs.SetInt   (LoggingString + lEnumItem + "_Message.FontIndex", lMessage.FontIndex);

			FontData lCallStack = m_Consts.EDebugTypeBaseFonts[lEnumItem].CallStack;
            PlayerPrefs.SetInt   (LoggingString + lEnumItem + "_CallStack.FontSize",    lCallStack.FontSize);
            PlayerPrefsX.SetColor(LoggingString + lEnumItem + "_CallStack.Colour",      lCallStack.Colour);
            PlayerPrefs.SetInt   (LoggingString + lEnumItem + "_CallStack.FontIndex",   lCallStack.FontIndex);
        }

        foreach (ETabStyle lEnumItem in Enum.GetValues(typeof(ETabStyle)))
        {
            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
            if (m_Consts.TabStyle.ContainsKey(lEnumItem) == false)
            {
                m_Consts.TabStyle.Add(lEnumItem, new FontData(Color.red));
            }
            FontData lFontData = m_Consts.TabStyle[lEnumItem];
            PlayerPrefsX.SetColor(LoggingString + lEnumItem + "_TabStyle.Colour", lFontData.Colour);
            PlayerPrefs.SetInt   (LoggingString + lEnumItem + "_TabStyle.FontStyle", (int)lFontData.FontStyle);
        }

		
        foreach (var lItemList in m_Consts.EDebugTypeAllList)
		{
            PlayerPrefsX.SetBool(LoggingString + "EDebugTypeAllList_" + lItemList.Key, lItemList.Value);
		}

        SaveEnumAllFlag();
    }

	/// <summary>
	/// Saves the enum all flag.
	/// </summary>
    public void SaveEnumAllFlag()
    {
        PlayerPrefs.SetInt(LoggingString + "_EnumAllFlag", m_Consts.EnumAllFlag);
    }
	
    public void Save_CscFile()
    {
        PlayerPrefs.SetString(LoggingString + "CSC_Directory", m_Consts.CSC_Directory);
    }
    /// <summary>
    /// 
    /// </summary>
	public void Load()
    {
        m_Consts.StoreColour           = PlayerPrefsX.GetColor(LoggingString + "StoreColour", m_Consts.StoreColour);
        m_Consts.SelectEmptyItem       = PlayerPrefsX.GetBool(LoggingString + "SelectEmptyItem", m_Consts.SelectEmptyItem);
        m_Consts.DisplayTime           = (EDisplayTime)PlayerPrefs.GetInt(LoggingString + "DisplayTime", (int)m_Consts.DisplayTime);
        m_Consts.CSC_Directory         = PlayerPrefs.GetString(LoggingString + "CSC_Directory", m_Consts.CSC_Directory);

        m_Consts.TabFont.FontIndex = PlayerPrefs.GetInt(LoggingString + "TabFont.FontIndex", m_Consts.TabFont.FontIndex);
        m_Consts.TabFont.FontSize  = PlayerPrefs.GetInt(LoggingString + "TabFont.FontSize", m_Consts.TabFont.FontSize);
        m_Consts.TabFont.FontStyle = (FontStyle)PlayerPrefs.GetInt(LoggingString + "TabFont.FontStyle", (int)m_Consts.TabFont.FontStyle);

        //--------------------------------------------------
        //      Safety Check
        //--------------------------------------------------
        if (m_Consts.TabFont.FontIndex < m_Consts.Fonts.Length)
        {
            m_Consts.TabFont.Font = m_Consts.Fonts[m_Consts.TabFont.FontIndex];
        }


		foreach (EDebugType lEnumItemTemp in Enum.GetValues(typeof(EDebugType)))
        {
			EDebugTypeBase lEnumItem = (EDebugTypeBase)lEnumItemTemp;
            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
            if (m_Consts.EDebugTypeBaseFonts.ContainsKey(lEnumItem) == false)
            {
                m_Consts.EDebugTypeBaseFonts.Add(lEnumItem, new EDebugTypeBaseFont());
            }
            FontData lMessage  = m_Consts.EDebugTypeBaseFonts[lEnumItem].Message;
            lMessage.FontSize  = PlayerPrefs.GetInt   (LoggingString + lEnumItem + "_Message.FontSize",  lMessage.FontSize);
            lMessage.Colour    = PlayerPrefsX.GetColor(LoggingString + lEnumItem + "_Message.Colour",    lMessage.Colour);
            lMessage.FontIndex = PlayerPrefs.GetInt   (LoggingString + lEnumItem + "_Message.FontIndex", lMessage.FontIndex);
            lMessage.FontStyle = (FontStyle)PlayerPrefs.GetInt(LoggingString + lEnumItem + "_Message.FontStyle", (int)lMessage.FontStyle);

            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
            if (lMessage.FontIndex < m_Consts.Fonts.Length)
            {
                lMessage.Font = m_Consts.Fonts[lMessage.FontIndex];
            }

            

            FontData lCallStack  = m_Consts.EDebugTypeBaseFonts[lEnumItem].CallStack;
            lCallStack.FontSize  = PlayerPrefs.GetInt   (LoggingString + lEnumItem + "_CallStack.FontSize", lCallStack.FontSize);
            lCallStack.Colour    = PlayerPrefsX.GetColor(LoggingString + lEnumItem + "_CallStack.Colour", lCallStack.Colour);
            lCallStack.FontIndex = PlayerPrefs.GetInt   (LoggingString + lEnumItem + "_CallStack.FontIndex", lCallStack.FontIndex);
            lCallStack.FontStyle =(FontStyle)PlayerPrefs.GetInt(LoggingString + lEnumItem + "_CallStack.FontStyle", (int)lCallStack.FontStyle);

            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
            if (lCallStack.FontIndex < m_Consts.Fonts.Length)
            {
                lCallStack.Font = m_Consts.Fonts[lCallStack.FontIndex];
            }

            m_Consts.EDebugTypeAllList.Clear();
            foreach(var lItem in  (EDebugTypeBase[])Enum.GetValues(typeof(EDebugTypeBase)))
            {
                bool lDefualt = false;               
                m_Consts.EDebugTypeAllList.Add(lItem, false);
                m_Consts.EDebugTypeAllList[lItem] = PlayerPrefsX.GetBool(LoggingString + "EDebugTypeAllList_" + lItem, lDefualt);
            }

			m_Consts.EnumAllFlag  = PlayerPrefs.GetInt   (LoggingString + "_EnumAllFlag", m_Consts.EnumAllFlag);       
        }


        foreach (ETabStyle lEnumItem in Enum.GetValues(typeof(ETabStyle)))
        {
            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
            if (m_Consts.TabStyle.ContainsKey(lEnumItem) == false)
            {
                m_Consts.TabStyle.Add(lEnumItem, new FontData(Color.red));
            }
            FontData lFontData  = m_Consts.TabStyle[lEnumItem];
            lFontData.Colour    = PlayerPrefsX.GetColor(LoggingString + lEnumItem + "_TabStyle.Colour", lFontData.Colour);
            lFontData.FontStyle = (FontStyle)PlayerPrefs.GetInt(LoggingString + lEnumItem + "_TabStyle.FontStyle", (int)lFontData.FontStyle);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <summary>
    /// 
    /// </summary>
    private void OnGUI()
    {
        if (m_Consts != null)
        {
            m_EDebugTypeBase = Tabs(m_EDebugTypeBase);
            FillInWindow(m_EDebugTypeBase);
            DisplayMessageCallStack();
            
        }
		
        if (m_Repaint == true || IsValidSearchText())
        {
            Repaint();
            m_Repaint = false;
        }
		
		if(m_Consts != null &&  m_Consts.AudioTracking == true)
		{
			AudioTracking();
		}
    }
	
	/// <summary>
	/// Audios the tracking.
	/// </summary>
	void AudioTracking()
	{
		AudioSource[] lSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		for (int i = 0; i < lSources.Length; i++)
		{
			if (lSources[i].clip != null && lSources[i].isPlaying)
			{
				AddToMessages(" Clip playing: " + lSources[i].clip.name, EDebugTypeBase.Audio , lSources[i].gameObject);
			}
		}
	}

	/// <summary>
	/// Raises the inspector update event.
	/// </summary>
    void OnInspectorUpdate()
    {

        if(m_LoadVariablesOnStart == false)
        {
            m_Consts = new Consts();
            CreateListOfFonts();
            InitiliseEDebugTypeBaseFonts();
            InilisizeTabTyle();
            Load();
			InitiliseGuiFormat();
            m_LoadVariablesOnStart = true;
            
        }
        if (m_Repaint == true || IsValidSearchText())
        {
            Repaint();
            m_Repaint = false;
        }
    }
	

    /// <summary>
    /// Initilises the GUI format.
    /// </summary>
    internal void InitiliseGuiFormat()
	{
		m_Consts.GuiInfo0 = new Helper.EditorGUILayout.EditorGUILayoutInfo( 0, m_Consts.SpaceAtBeginning * 0, 0, 10, m_Consts.InformationHeight);
        m_Consts.GuiInfo1 = new Helper.EditorGUILayout.EditorGUILayoutInfo( 1, m_Consts.SpaceAtBeginning * 0, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
		m_Consts.GuiInfo2 = new Helper.EditorGUILayout.EditorGUILayoutInfo( 1, m_Consts.SpaceAtBeginning * 1, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
        m_Consts.GuiInfo3 = new Helper.EditorGUILayout.EditorGUILayoutInfo( 1, m_Consts.SpaceAtBeginning * 2, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
        m_Consts.GuiInfo4 = new Helper.EditorGUILayout.EditorGUILayoutInfo( 1, m_Consts.SpaceAtBeginning * 3, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
        m_Consts.GuiInfo5 = new Helper.EditorGUILayout.EditorGUILayoutInfo( 1, m_Consts.SpaceAtBeginning * 4, m_Consts.NameWidth, m_Consts.InformationWidth, m_Consts.InformationHeight);
    }

    /// <summary>
    /// 
    /// </summary>
    internal void InilisizeTabTyle()
    {
        foreach (ETabStyle lEnumItem in Enum.GetValues(typeof(ETabStyle)))
        {
            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
            if (m_Consts.TabStyle.ContainsKey(lEnumItem) == false)
            {
                m_Consts.TabStyle.Add(lEnumItem, new FontData());
            }
        }

        m_Consts.TabStyle[ETabStyle.HighLight].Colour = new Color(0.3f, 0.3f, 0.3f);
        m_Consts.TabStyle[ETabStyle.Normal].Colour    = new Color(0.5f, 0.5f, 0.5f);
        m_Consts.TabStyle[ETabStyle.NewItem].Colour   = Helper.Colour.Colour256ToColour(210, 36, 46);
		m_Consts.TabStyle[ETabStyle.NoNewItem].Colour = Helper.Colour.Colour256ToColour(11, 196, 89); // nice green
    }


    /// <summary>
    /// 
    /// </summary>
    internal void InitiliseEDebugTypeBaseFonts()
    {
		foreach (EDebugType lEnumItemTemp in Enum.GetValues(typeof(EDebugType)))
        {
			EDebugTypeBase lEnumItem = (EDebugTypeBase)lEnumItemTemp;
            //--------------------------------------------------
            //      Safety Check
            //--------------------------------------------------
            if (m_Consts.EDebugTypeBaseFonts.ContainsKey(lEnumItem) == false)
            {
                m_Consts.EDebugTypeBaseFonts.Add(lEnumItem, new EDebugTypeBaseFont());
            }
        }
    }


	/// <summary>
	/// Determines whether this instance is valid search text.
	/// </summary>
    bool IsValidSearchText()
    {
        if (m_SearchMenu != null && m_SearchMenu.m_Consts.m_SearchText != "")
        {
            return true;
        }
        return false;
    }

	/// <summary>
	/// Determines whether this instance is valid search text the specified lString.
	/// </summary>
    bool IsValidSearchText(ref string lString)
    {
        if(m_SearchMenu != null && m_SearchMenu.m_Consts.m_SearchText != "")
        {
            lString = m_SearchMenu.m_Consts.m_SearchText;
            return true;
        }
        return false;
    }
	
    /// <summary>
    /// 
    /// </summary>
    private void FillInWindow(EDebugTypeBase lEDebugType)
    {
        // reset BackGround
        GUI.backgroundColor  = Color.white;
		m_Consts.m_ScrollPos = EditorGUILayout.BeginScrollView(m_Consts.m_ScrollPos, true, true);

		Helper.EditorGUILayout.Space(3);

		DisplayAllEnumButton(lEDebugType);


		List<Data> lDatas = null;

		if(m_Consts.Compressed == true)
		{
			lDatas = m_DictionaryListCountData.GetList(lEDebugType);
		}
		else
		{
			lDatas = m_DictionaryListData.GetList(lEDebugType);
		}

        
        for (int i = 0; i < lDatas.Count; i++)
        {
			Data lData = lDatas[i];
			if(ShouldDisplayData(lData) == true)
			{
            	//EditorGUILayout.BeginHorizontal();
				DisplayMessageString(lData);
            	DisplayMessageGameObjectName(lData);
                

               // EditorGUILayout.EndHorizontal();
				
            	
			}

        }
		DisplayClearButton(lEDebugType, ref lDatas);

        EditorGUILayout.EndScrollView();
    }


	/// <summary>
	/// Displaies the data.
	/// </summary>
	private bool ShouldDisplayData(Data lData)
	{
		string lSearchString = "";            
		bool lValidSearch    = IsValidSearchText(ref lSearchString);          
		bool lFound          = true;
		
        if(lValidSearch == true)
		{
			lFound = false;
			switch(m_SearchMenu.m_Consts.SearchType)
			{
			case SearchMenu.ESearchType.Message:
				lFound = lData.m_Message.Contains(lSearchString, m_SearchMenu.m_Consts.CaseSensive);
				break;
				
			case SearchMenu.ESearchType.CallStack:
				lFound = lData.m_CallStack.Contains(lSearchString, m_SearchMenu.m_Consts.CaseSensive);
				break;
				
			case SearchMenu.ESearchType.GameObjectName:
				lFound = lData.m_GameObjectNameFull.Contains(lSearchString, m_SearchMenu.m_Consts.CaseSensive);
				break;
				
			case SearchMenu.ESearchType.All:
				lFound = lData.m_Message.Contains(lSearchString, m_SearchMenu.m_Consts.CaseSensive);
				if(lFound == false)
				{
                        lFound = lData.m_CallStack.Contains(lSearchString, m_SearchMenu.m_Consts.CaseSensive);
                    }
                    if(lFound == false)
                    {
                        lFound = lData.m_GameObjectNameFull.Contains(lSearchString, m_SearchMenu.m_Consts.CaseSensive);
                    }
                    break;
            }
        }
		return lFound;
    }

    /// <summary>
    /// Displaies all enum button.
	/// </summary>
	private void DisplayAllEnumButton(EDebugTypeBase lEDebugType)
	{
        if (lEDebugType == EDebugTypeBase.All)
		{
			const int lSpaceAtBeginning = 20;
			const int lNameWidth        = 150;
			const int lInformationWidth = 150;
            const int lNameHieght       = 20;
            Helper.EditorGUILayout.EditorGUILayoutInfo lInfo1 = new Helper.EditorGUILayout.EditorGUILayoutInfo(2, lSpaceAtBeginning * 1, lNameWidth, lInformationWidth, lNameHieght);

            Dictionary <EDebugTypeBase, bool> EDebugTypeAllListTemp = new Dictionary<EDebugTypeBase, bool>(m_Consts.EDebugTypeAllList);

            m_Consts.EDebugTypeAllList = Helper.EditorGUILayout.EnumDictionaryExcept<EDebugTypeBase, EDebugSpecailList>("EnumList", ref m_Consts.EnumAllFlag, lInfo1, true);

            if (Helper.DictionaryComparer<EDebugTypeBase, bool>(EDebugTypeAllListTemp, m_Consts.EDebugTypeAllList) == false)
			{
				Remake_TabAll();
				SaveEnumAllFlag();				
			}

            EditorGUILayout.BeginHorizontal();
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
            EditorGUILayout.EndHorizontal();
            Helper.EditorGUILayout.Space(1);			
		}
	}

	/// <summary>
	/// Displaies the message string.
	/// </summary>
	private void DisplayMessageString(Data lData)
	{
        EditorGUILayout.BeginHorizontal();
        GUI.backgroundColor = Color.white;
        if (m_Consts.SelectedItem == lData)
        {
            GUI.backgroundColor = Color.green;
        }
        if (GUILayout.Button("", GUILayout.Width(10), GUILayout.Height(10)) == true)
        {
            if (lData.m_GameObject != null)
            {                
                Selection.activeGameObject    = lData.m_GameObject;
                EditorGUIUtility.PingObject(Selection.activeGameObject);
            }
            m_Consts.SelectedItem = lData;
        }
        GUI.backgroundColor = Color.white;
        //--------------------------------------------------
        //      Safety Check
        //--------------------------------------------------
        if (m_Consts.EDebugTypeBaseFonts.ContainsKey(lData.m_EDebugType) == false)
		{
            m_Consts.EDebugTypeBaseFonts.Add((EDebugTypeBase)lData.m_EDebugType, new EDebugTypeBaseFont());
		}
		
		GUIStyle lMyButtonStyle         = new GUIStyle();

		lMyButtonStyle.fontSize         = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.FontSize;
		lMyButtonStyle.font             = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.Font;
		lMyButtonStyle.fontStyle        = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.FontStyle;
        lMyButtonStyle.normal.textColor = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.Colour;
        string lTimeString = "";
		switch(m_Consts.DisplayTime)
		{
		case EDisplayTime.NoTime:
			lTimeString = "";
			break;
			
		case EDisplayTime.FromStart:
			lTimeString = "Time " + lData.m_TimeFromStart.ToString();
			break;
			
		case EDisplayTime.RealTime:
			lTimeString = "Time " + String.Format("{0:HH:mm:ss.ffffff}", lData.m_DateTime);
			break;
			
		case EDisplayTime.RealTimeShort:
			lTimeString = "Time " + String.Format("{0:mm:ss.fff}", lData.m_DateTime);
			break;
		}
        EditorGUILayout.LabelField("", GUILayout.Width(m_Consts.SelectButtonGap));
        DisplayCompressed(lData, m_Consts.Compressed);
        EditorGUILayout.TextArea(lTimeString.PadRight(m_Consts.PadRightTimeGameobject) + "  " + lData.m_Message, lMyButtonStyle, GUILayout.Width(1000));
        
        EditorGUILayout.EndHorizontal();
	}

	/// <summary>
	/// Displaies the name of the message game object.
	/// </summary>
	void DisplayMessageGameObjectName(Data lData)
	{
        string lNameString = "GameObject -->>   ";
		
		GUIStyle lMyButtonObjectNameStyle         = new GUIStyle();
		lMyButtonObjectNameStyle.normal.textColor = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.Colour;
		lMyButtonObjectNameStyle.fontSize         = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.FontSize;
		lMyButtonObjectNameStyle.font             = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.Font;
		lMyButtonObjectNameStyle.fontStyle        = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.FontStyle;

        EditorGUILayout.BeginHorizontal();

        if (m_Consts.Compressed == true)
        {
            Helper.EditorGUILayout.LabelFieldBlank(38);
        }
        Helper.EditorGUILayout.LabelFieldBlank(10 + m_Consts.SelectButtonGap);
        EditorGUILayout.TextArea(lNameString.PadRight(m_Consts.PadRightTimeGameobject) + lData.m_GameObjectNameFull, lMyButtonObjectNameStyle, GUILayout.Width(300));
        EditorGUILayout.EndHorizontal();
	}
	
	/// <summary>
	/// Displaies the compressed.
	/// </summary>
	void DisplayCompressed(Data lData, bool lCompressed)
	{
		if(lCompressed == true)
		{
			GUIStyle lMyButtonObjectNameStyle         = new GUIStyle();
			lMyButtonObjectNameStyle.normal.textColor = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.Colour;
			lMyButtonObjectNameStyle.fontSize         = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.FontSize;
			lMyButtonObjectNameStyle.font             = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.Font;
			lMyButtonObjectNameStyle.fontStyle        = m_Consts.EDebugTypeBaseFonts[lData.m_EDebugType].Message.FontStyle;
            string lNumber = "(" + lData.m_Count.ToString() + ")";
            EditorGUILayout.TextArea(lNumber.PadRight(10), lMyButtonObjectNameStyle, GUILayout.Width(40));
		}
	}

	/// <summary>
	/// Displaies the message call stack.
	/// </summary>
	void DisplayMessageCallStack()
	{
        EditorGUILayout.Space();
        m_Consts.m_ScrollPosCallStack = EditorGUILayout.BeginScrollView(m_Consts.m_ScrollPosCallStack, true, true, GUILayout.Height(70));

        if (m_Consts.SelectedItem != null)
        {
            //------------------------------------------------------------------------------
            //        if its checked, the display all the stack trace
            //------------------------------------------------------------------------------

            GUIStyle lCallStackButtonStyle = new GUIStyle();

            lCallStackButtonStyle.normal.textColor = m_Consts.EDebugTypeBaseFonts[m_Consts.SelectedItem.m_EDebugType].CallStack.Colour;
            lCallStackButtonStyle.fontSize = m_Consts.EDebugTypeBaseFonts[m_Consts.SelectedItem.m_EDebugType].CallStack.FontSize;
            lCallStackButtonStyle.font = m_Consts.EDebugTypeBaseFonts[m_Consts.SelectedItem.m_EDebugType].CallStack.Font;
            lCallStackButtonStyle.fontStyle = m_Consts.EDebugTypeBaseFonts[m_Consts.SelectedItem.m_EDebugType].CallStack.FontStyle;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("", GUILayout.Width(m_Consts.ButtonStartWidth / 2));
            EditorGUILayout.TextArea(m_Consts.SelectedItem.m_CallStack, lCallStackButtonStyle);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
        }
        EditorGUILayout.EndScrollView();

    }

	/// <summary>
	/// Displaies the clear button.
	/// </summary>
	void DisplayClearButton(EDebugTypeBase lEDebugType, ref List<Data> lDatas)
	{
		//------------------------------------------------------------------------------
		//                      If there is information then present the button
		//------------------------------------------------------------------------------
		if (lDatas.Count > 0)
		{
			Helper.EditorGUILayout.Space(4);
			EditorGUILayout.BeginHorizontal();
			int lSpace = Helper.ButtonSpaceCentre(m_Consts.ButtonWidth, (int)position.width, true);
			
			Helper.EditorGUILayout.LabelFieldBlank(lSpace);
			
			if (GUILayout.Button(m_Consts.ClearButtonText, GUILayout.Width(m_Consts.ButtonWidth), GUILayout.Height(m_Consts.ButtonHeight)) == true)
			{
                //------------------------------------------------------------------------------
                //        Clear the data
                //------------------------------------------------------------------------------

                if (lEDebugType == EDebugTypeBase.All)
                {
                    m_DictionaryListData.ClearAll();

                    List<EDebugTypeBase> lKeys = new List<EDebugTypeBase>(m_DictionaryListLastViewdCount.Keys);
                    foreach (EDebugTypeBase lKey in lKeys)
                    {
                        m_DictionaryListLastViewdCount[lKey] = 0;
                    }
                }
                else
                {
                    m_DictionaryListData.ClearList(lEDebugType);
                    m_DictionaryListLastViewdCount[lEDebugType] = 0;
                }


				Remake_TabAll();
			}
			
			EditorGUILayout.EndHorizontal();
            Helper.EditorGUILayout.Space(2);
        }
	}


    /// <summary>
    /// 
    /// <returns></returns>
    private EDebugTypeBase Tabs(EDebugTypeBase lSelectedItem)
    {
        string lTick  = ((char)0x221A).ToString();
        string lXross = "x";
		GUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("", GUILayout.Width(position.width - (m_Consts.ButtonWidth * 2) - 10 - 30 - 25 - 25 - 10));
        string lAudioTrackingString = "A";
        if(m_Consts.AudioTracking == true)
        {

            lAudioTrackingString += lTick;
        }
        else
        {
            lAudioTrackingString += lXross;
        }

        if (GUILayout.Button(lAudioTrackingString, GUILayout.Width(30), GUILayout.Height(20)))
		{
			m_Consts.AudioTracking = EditorUtility.DisplayDialog("Audio Trackinge?",
			                                                     "Do you wish to Enable Audio Tracking ", 
			                                                     "Enable", 
			                                                     "Disable");
			Repaint();
		}


	
		if (GUILayout.Button("C", GUILayout.Width(20), GUILayout.Height(20)))
		{
			m_Consts.Compressed = !m_Consts.Compressed;
			Repaint();
		}



		if (GUILayout.Button("Search Menu!", GUILayout.Width(m_Consts.ButtonWidth), GUILayout.Height(20)))
		{
			Repaint();
            if (m_SearchMenu == null)
            {
                m_SearchMenu = new SearchMenu(this);
            }
		}

		if (GUILayout.Button("Settings Menu!", GUILayout.Width(m_Consts.ButtonWidth), GUILayout.Height(20)))
		{
			Repaint();
            if (m_Settings == null)
            {
                m_Settings = new Settings(this);
            }
        }
        GUILayout.EndHorizontal();


		m_Consts.m_ScrollPosTab = EditorGUILayout.BeginScrollView(m_Consts.m_ScrollPosTab, true, false, GUILayout.Height(55));
		m_Consts.m_ScrollPosTab = new Vector2(m_Consts.m_ScrollPosTab.x, 0);
        GUILayout.Space(m_Consts.StartSpace);

        GUIStyle buttonStyle       = new GUIStyle(GUI.skin.button);
        buttonStyle.padding.bottom = 8;
        buttonStyle.font           = (m_Consts.TabFont.Font != null) ? m_Consts.TabFont.Font : buttonStyle.font;
        buttonStyle.fontSize       = m_Consts.TabFont.FontSize;
        buttonStyle.fontStyle      = m_Consts.TabFont.FontStyle;

        
        GUILayout.BeginHorizontal();
        {   //Create a row of buttons
            foreach (EDebugTypeBase lEnumItem in Enum.GetValues(typeof(EDebugTypeBase)))
            {
                ETabStyle lETabStyle     = ETabStyle.Normal;
                int lLastViewedCount     = m_DictionaryListData.Count(lEnumItem);
                bool lIsButtonSelectable = (lLastViewedCount > 0);
                int lDiffentAmountSinceLastViewd   = Mathf.Abs(m_DictionaryListLastViewdCount[lEnumItem] - lLastViewedCount);

                //--------------------------------------------------
                //  sets back ground colour
                //--------------------------------------------------
                if (lIsButtonSelectable == false)
                {
                    lETabStyle = ETabStyle.NoNewItem;
                }

                if (m_DictionaryListLastViewdCount[lEnumItem] != lLastViewedCount)
                {
                    lETabStyle = ETabStyle.NewItem;
                }

                if (lSelectedItem == lEnumItem)
                {
                    lETabStyle = ETabStyle.HighLight;
                }

                if (m_Consts.TabStyle.ContainsKey(lETabStyle) == true)
                {
                    GUI.backgroundColor = m_Consts.TabStyle[lETabStyle].Colour;
                    buttonStyle.fontStyle = m_Consts.TabStyle[lETabStyle].FontStyle;
                }
                //---------------------------------------------------------------------------------
                //    Create the button name, with a number of there are new items
                //---------------------------------------------------------------------------------
				string lButtonName = (lLastViewedCount == 0) ? lEnumItem.ToString() : lEnumItem.ToString() + "(" + lLastViewedCount.ToString() + ")";

                if (GUILayout.Button(lButtonName, buttonStyle) == true && 
                    (	lIsButtonSelectable == true  || 
				 		m_Consts.SelectEmptyItem == true ||
				 		Helper.Enum.IsEnumInEnumItem<EDebugSpecailList, EDebugTypeBase>(lEnumItem) == true
				 ))
                {
                    m_DictionaryListLastViewdCount[lEnumItem] = lLastViewedCount;
                    
                    //---------------------------------------------------------------------------------
                    //    if click on a the tab AGAIN then it will make the items all apear or disapear
                    //---------------------------------------------------------------------------------
                    if (lSelectedItem != lEnumItem)
                    {
                        m_Consts.SelectedItem = null;
                    }
                    lSelectedItem = lEnumItem;
                }
            }
        }
        Helper.EditorGUILayout.LabelFieldBlank(10); // gap at the tend were the scrollbar is
        GUILayout.EndHorizontal();
        EditorGUILayout.EndScrollView();

        return lSelectedItem;
    }

    private SearchMenu m_SearchMenu;
    private Settings m_Settings;

    /// <summary>
    /// 
    /// </summary>
    internal void CreateListOfFonts()
    {
        //----------------------------------------------
        //          Safety Checks
        //----------------------------------------------
        List<Font> lTemp     = new List<Font>();
        m_Consts.Fonts       = Resources.FindObjectsOfTypeAll(typeof(Font)) as Font[];
        m_Consts.FontStrings = new List<string>();
        foreach (Font lFont in m_Consts.Fonts)
        {
            if (lFont.fontNames.Length > 0)
            {
                if (m_Consts.FontStrings.Contains(lFont.fontNames[0]) == false)
                {
                    m_Consts.FontStrings.Add(lFont.fontNames[0]);
                    lTemp.Add(lFont);
                }
            }
        }       
    }

    /// <summary>
    /// 
    /// </summary>




    #endregion

    ///

}

