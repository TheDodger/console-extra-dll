﻿
using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

public class Settings : EditorWindow
{

    private EnumMenu m_EnumMenu;
    public static Settings m_Window = null;
	Vector2 m_ScrollPosTab = Vector2.zero;
    Logging m_Logging;
    public  Settings(Logging lLogging)
	{
		m_Window         = (Settings)EditorWindow.GetWindow(typeof(Settings), true, "Settings Menu");
		m_Window.maxSize = new Vector2(215f * 2.2f, 110.0f * 5.0f);
		m_Window.minSize = m_Window.maxSize;
        m_Logging        = lLogging;
    }

	private void OnGUI()
	{
		OnInspectorUpdate();
		FillInWindowSetting();
		Repaint();
		Logging.m_Repaint = true;
	}
	
	/// <summary>
	/// 
	/// </summary>
	void OnInspectorUpdate()
	{
		if (m_Window == null)
		{
			m_Window = (Settings)EditorWindow.GetWindow(typeof(Settings), true, "Settings Menu");
		}

		Repaint();
		Logging.m_Repaint = true;
	}

    /// <summary>
    /// 
    /// </summary>
    void FillInWindowSetting()
	{
		if(m_Logging == null || m_Logging.m_Consts ==  null)
		{
			this.Close();
		}

		m_ScrollPosTab      = EditorGUILayout.BeginScrollView(m_ScrollPosTab, true, false);
		GUI.backgroundColor = Color.white;
		
		Helper.EditorGUILayout.Space(3);
        int lInt = Helper.EditorGUILayout.PopupEnum("Message Type", Enum.GetNames(typeof(ConsoleExtraEnum.EDebugTypeBase)), (int)m_Logging.m_Consts.EDebugTypeBaseFontsChoice, m_Logging.m_Consts.GuiInfo1);
        m_Logging.m_Consts.EDebugTypeBaseFontsChoice = (ConsoleExtraEnum.EDebugTypeBase)lInt;

        //--------------------------------------------------
        //      Safety Check
        //--------------------------------------------------
        if (m_Logging.m_Consts.EDebugTypeBaseFonts.ContainsKey(m_Logging.m_Consts.EDebugTypeBaseFontsChoice) == false)
		{
            m_Logging.m_Consts.EDebugTypeBaseFonts.Add(m_Logging.m_Consts.EDebugTypeBaseFontsChoice, new Logging.EDebugTypeBaseFont());
		}
		
		//--------------------------------------------------------
		//          Messages
		//--------------------------------------------------------
		Logging.FontData lMessage = m_Logging.m_Consts.EDebugTypeBaseFonts[m_Logging.m_Consts.EDebugTypeBaseFontsChoice].Message;
		
		Helper.EditorGUILayout.LabelField("Message", m_Logging.m_Consts.GuiInfo2);
		Helper.EditorGUILayout.Popup("Message Font",  ref m_Logging.m_Consts.FontStrings, ref lMessage.FontIndex, m_Logging.m_Consts.GuiInfo3);
		
		//----------------------------------------------
		//          Safety Checks
		//----------------------------------------------
		if (lMessage.FontIndex < m_Logging.m_Consts.Fonts.Length)
		{
			lMessage.Font = m_Logging.m_Consts.Fonts[lMessage.FontIndex];
		}
		
		Helper.EditorGUILayout.IntSlider ("Message Font Size",   ref lMessage.FontSize , 3, 20, m_Logging.m_Consts.GuiInfo3);
		Helper.EditorGUILayout.ColorField("Message Font Colour", ref lMessage.Colour, m_Logging.m_Consts.GuiInfo3);
		lMessage.FontStyle = (FontStyle)Helper.EditorGUILayout.PopupEnum("Message Font Style", Enum.GetNames(typeof(FontStyle)), (int)lMessage.FontStyle, m_Logging.m_Consts.GuiInfo3);
		//-----------------------------------------------------------------
		
		//--------------------------------------------------------
		//          lCallStack
		//--------------------------------------------------------
		{
			Logging.FontData lCallStack = m_Logging.m_Consts.EDebugTypeBaseFonts[m_Logging.m_Consts.EDebugTypeBaseFontsChoice].CallStack;
			
			Helper.EditorGUILayout.LabelField ("CallStack", m_Logging.m_Consts.GuiInfo2);
			Helper.EditorGUILayout.Popup ("CallStack Font",            ref m_Logging.m_Consts.FontStrings, ref lCallStack.FontIndex, m_Logging.m_Consts.GuiInfo3);
			Helper.EditorGUILayout.IntSlider   ("CallStack Font Size", ref lCallStack.FontSize,  3, 20, m_Logging.m_Consts.GuiInfo3);
			Helper.EditorGUILayout.ColorField("CallStack Font Colour", ref lCallStack.Colour, m_Logging.m_Consts.GuiInfo3);
			lCallStack.FontStyle = (FontStyle)Helper.EditorGUILayout.PopupEnum("CallStack Font Style", Enum.GetNames(typeof(FontStyle)), (int)lCallStack.FontStyle, m_Logging.m_Consts.GuiInfo3);
		}
		
		Helper.EditorGUILayout.Space(5);
		Helper.EditorGUILayout.LabelField("Tab", m_Logging.m_Consts.GuiInfo1);
		Helper.EditorGUILayout.Popup("Tab Fonts", ref m_Logging.m_Consts.FontStrings, ref m_Logging.m_Consts.TabFont.FontIndex, m_Logging.m_Consts.GuiInfo2);
		
		//--------------------------------------------------
		//      Safety Check
		//--------------------------------------------------
		if (m_Logging.m_Consts.Fonts != null)
		{
			if (m_Logging.m_Consts.TabFont.FontIndex < m_Logging.m_Consts.Fonts.Length)
			{
                m_Logging.m_Consts.TabFont.Font = m_Logging.m_Consts.Fonts[m_Logging.m_Consts.TabFont.FontIndex];
			}            
		}
		
		
		Helper.EditorGUILayout.IntSlider("Tab Font Size", ref m_Logging.m_Consts.TabFont.FontSize, 2, 20, m_Logging.m_Consts.GuiInfo2);
		
		Logging.FontData lFontData     = m_Logging.m_Consts.TabStyle[m_Logging.m_Consts.TabStyleIndex];
        m_Logging.m_Consts.TabStyleIndex = (Logging.ETabStyle)Helper.EditorGUILayout.PopupEnum("Tab Type", Enum.GetNames(typeof(Logging.ETabStyle)), (int)m_Logging.m_Consts.TabStyleIndex, m_Logging.m_Consts.GuiInfo2);
		lFontData.FontStyle    = (FontStyle)Helper.EditorGUILayout.PopupEnum("Font Style", Enum.GetNames(typeof(FontStyle)), (int)lFontData.FontStyle, m_Logging.m_Consts.GuiInfo3);
		Helper.EditorGUILayout.ColorField("Font Colour", ref lFontData.Colour, m_Logging.m_Consts.GuiInfo3);
		
		Helper.EditorGUILayout.Space(4);
		
		Helper.EditorGUILayout.Toggle("SelectEmptyItem", ref m_Logging.m_Consts.SelectEmptyItem, m_Logging.m_Consts.GuiInfo1);
        m_Logging.m_Consts.DisplayTime = (Logging.EDisplayTime)Helper.EditorGUILayout.PopupEnum("Display Time", Enum.GetNames(typeof(Logging.EDisplayTime)), (int)m_Logging.m_Consts.DisplayTime, m_Logging.m_Consts.GuiInfo1);
		
		Helper.EditorGUILayout.Space(1);
		//----------------------------------------------------------------------
		Helper.EditorGUILayout.Space(4);

        EditorGUILayout.BeginHorizontal();
        int lMiddleWidth = Helper.ButtonSpace(m_Logging.m_Consts.ButtonStartWidth, m_Logging.m_Consts.ButtonWidth, 1, position.width, true);
        Helper.EditorGUILayout.LabelFieldBlank(lMiddleWidth);
        if (GUILayout.Button("Enums!", GUILayout.Width(m_Logging.m_Consts.ButtonWidth), GUILayout.Height(m_Logging.m_Consts.ButtonHeight)))
        {
            if (m_EnumMenu == null)
            {
                m_EnumMenu = new EnumMenu(m_Logging, this);
            }
            Repaint();
        }
        EditorGUILayout.EndHorizontal();
        Helper.EditorGUILayout.Space(2);
        EditorGUILayout.BeginHorizontal();
		{
			lMiddleWidth        = Helper.ButtonSpace(m_Logging.m_Consts.ButtonStartWidth, m_Logging.m_Consts.ButtonWidth, 2, position.width, true);
			
			EditorGUILayout.LabelField("", GUILayout.Width(m_Logging.m_Consts.ButtonStartWidth));
			if (GUILayout.Button("Save!", GUILayout.Width(m_Logging.m_Consts.ButtonWidth), GUILayout.Height(m_Logging.m_Consts.ButtonHeight)))
			{
                m_Logging.Save();
				Repaint();
			}
			
			Helper.EditorGUILayout.LabelFieldBlank(lMiddleWidth);
			
			if (GUILayout.Button("Revert!", GUILayout.Width(m_Logging.m_Consts.ButtonWidth), GUILayout.Height(m_Logging.m_Consts.ButtonHeight)))
			{
                m_Logging.Load();
				Repaint();
			}
		}
		EditorGUILayout.EndHorizontal();
		
		Helper.EditorGUILayout.Space(3);

		EditorGUILayout.EndScrollView();
	}
}

