﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;




public class ConsoleExtra : MonoBehaviour
{
#if UNITY_EDITOR
    public static void Log(string lMessage, GameObject lGameObject, ConsoleExtraEnum.EDebugType lEDebugType)
    {
        LoggingExtra.Log(lMessage, lGameObject, lEDebugType);
    }
#else
    public static void Log(string lMessage, GameObject lGameObject, ConsoleExtraEnum.EDebugType lEDebugType)
    {
        //LoggingExtra.Log(lMessage, lGameObject, lEDebugType);
    }
#endif
}
