//#define START
#if START

namespace ConsoleExtraEnum
{
    public enum EDebugTypeBase
    {
        All,
        Audio,
    }

    public enum EDebugType{}
    public enum LogError{}

    public enum EDebugSpecailList
    {
        All = EDebugTypeBase.All,
        Audio = EDebugTypeBase.Audio,
    }
}

#else

namespace ConsoleExtraEnum
{
  public enum EDebugTypeBase
  {
      All,
      Audio,    
      
      Init,
      InitAndFixed,
      Contact,
      Rotation,
      Disabled,
      Destroyed,
      Finish,
  }



  public enum EDebugType
  {
      
      Init = EDebugTypeBase.Init,
      InitAndFixed = EDebugTypeBase.InitAndFixed,
      Contact = EDebugTypeBase.Contact,
      Rotation = EDebugTypeBase.Rotation,
      Disabled = EDebugTypeBase.Disabled,
      Destroyed = EDebugTypeBase.Destroyed,
      Finish = EDebugTypeBase.Finish,
  }



  public enum LogError
  {
      Audio = EDebugTypeBase.Audio,
      
      Init = EDebugTypeBase.Init,
      Contact = EDebugTypeBase.Contact,
      Rotation = EDebugTypeBase.Rotation,
      Disabled = EDebugTypeBase.Disabled,
      Finish = EDebugTypeBase.Finish,
      
  }


  public enum EDebugSpecailList
  {
      All   = EDebugTypeBase.All,
      Audio = EDebugTypeBase.Audio,
  }
}
#endif